package com.aotero.laboratorio03kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registro_exitoso.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegistrar.setOnClickListener {

            val nombres = edtNombres.text.toString()
            val edad = edtEdad.text.toString()

            if(nombres.isEmpty()){
                Toast.makeText(this,"Debe Ingresar el nombre de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if(edad.isEmpty()){
                Toast.makeText(this,"Debe Ingresar la edad de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }



            var mascota= when {
                rbPerro.isChecked -> {
                    "Perro"
                }
                rbGato.isChecked -> {
                    "Gato"
                }
                else -> {
                     "Conejo"
                }
            }

            var vacunas=""

            if(chkVacuna1.isChecked){
                vacunas+="Vacuna contra el moquillo \n"
            }
            if(chkVacuna2.isChecked){
                vacunas+="Vacuna contra el parvovirus \n"
            }
            if(chkVacuna3.isChecked){
                vacunas+="Vacuna contra la hepatitis infecciosa \n"
            }
            if(chkVacuna4.isChecked){
                vacunas+="Vacuna contra la leptospirosis \n"
            }
            if(chkVacuna5.isChecked){
                vacunas+="Vacuna contra el moquillo \n"
            }




            val bundle =Bundle().apply {
                putString("key_nombres",nombres)
                putString("key_edad",edad)
                putString("key_mascota",mascota)
                putString("key_vacunas",vacunas)


            }

            val intent= Intent(this,RegistroExitosoActivity::class.java).apply {
                //Envia la informacion guardad en el bundle
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }
}