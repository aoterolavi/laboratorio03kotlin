package com.aotero.laboratorio03kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registro_exitoso.*

class RegistroExitosoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_exitoso)

        val bundleRecepcion : Bundle? = intent.extras

        bundleRecepcion?.let {
            val nombres =it.getString("key_nombres","")?:""
            val edad = it.getString("key_edad","")?:""
            val mascota =it.getString("key_mascota","")?:""
            val vacunas=it.getString("key_vacunas","")?:""


            when {
                mascota.equals("Perro") -> {
                    imgMascota.setImageResource(R.drawable.icono_perro_grande)
                }
                mascota.equals("Gato") -> {
                    imgMascota.setImageResource(R.drawable.icono_gato_grande)
                }
                else -> {
                    imgMascota.setImageResource(R.drawable.icono_conejo_grande)
                }
            }

            tvNombres.text= "Nombres : $nombres"
            tvEdad.text= "Edad : $edad"
            tvMascota.text= "Tipo de Mascota : $mascota"
            tvVacunas.text= "Vacunas : \n$vacunas"
        }

    }

}